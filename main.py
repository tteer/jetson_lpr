import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
import numpy as np

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages, letterbox
from utils.general import (
    check_img_size, non_max_suppression, apply_classifier, scale_coords,
    xyxy2xywh, plot_one_box, strip_optimizer, set_logging)

from models.character_reader.character_reader import Character_Reader
from models.character_detector.character_detector import Character_Detector
from models.plate_detector.plate_detector import Plate_Detector

import warnings
warnings.filterwarnings("ignore", category=UserWarning, module="torch.nn.functional")



def detect():

    input_dir = "images/input"
    img_size = 640
    threshold = [0.4, 0.2, 0.4]
    ### Initialize ###
    device = torch.device('cpu')
    dataset = LoadImages(input_dir, img_size=img_size)
    save_path = "images/output/result"

    ### Loading Models ###
    plate_model_path = "models/plate_detector/lp_detector_200.pt"
    plate_det_model = Plate_Detector(plate_model_path, device, threshold[0])

    char_det_model_path = "models/character_detector/text_detector_201.pt"
    char_det_model = Character_Detector(char_det_model_path, device, threshold[1])

    #img0 = cv2.imread(input_dir)
    #img = convert(img0, img_size)
    for path, img, im0s, vid_cap in dataset:
        ### Plate Detection ###
        processed_img = plate_det_model.preprocess(img)
        print(processed_img.shape)
        pred = plate_det_model.inference(processed_img)
        plate_img = plate_det_model.postprocess(pred, processed_img, im0s)
        
        #cv2.imwrite(save_path + str(1)+".jpg", plate_img)
        ### Conversion ###
        shape = (im0s.shape[0], im0s.shape[1])
        plate_img = cv2.resize(plate_img, shape)
        
        img = convert(plate_img, img_size)

        ### Character Detection ###
        processed_img = char_det_model.preprocess(img)
        pred = char_det_model.inference(processed_img)
        char_img = char_det_model.postprocess(pred, processed_img, plate_img)
        

        ### Conversion ###
        shape = (im0s.shape[0], im0s.shape[1])
        char_img = cv2.resize(char_img, shape)
        

        ### Character Reader ###
        conf_path = "models/character_reader/model_config.yaml"
        char_read_model = Character_Reader(conf_path, threshold[2], cuda_device=0)
        images = [char_img]
        processed_img = char_read_model._preprocess(images)
        #print(processed_img.type())
        pred = char_read_model._predict(processed_img)
        result = char_read_model._postprocess(pred)
        
        if len(result['character']) != 0:
            print("\n"+"Prediction: " + result['character'][0])
            print("Confidence: " + str(result['confidence'][0]))
        else:
            print("\n" + "Plate not found")
        
    

def convert(base_img, img_size):
    img = letterbox(base_img, new_shape=img_size)[0]
    img = img[:, :, ::-1].transpose(2, 0, 1)
    img = np.ascontiguousarray(img)
    return img

if __name__ == "__main__":
    detect()