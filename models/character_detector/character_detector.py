from models.experimental import attempt_load
from utils.general import non_max_suppression, scale_coords
import torch
import torchvision
class Character_Detector():

    def __init__(self, weights, device, threshold):
        
        self.device = device
        #self.model = torch.load(path, map_location=device)['model'].float().fuse().eval()
        self.model = attempt_load(weights, map_location=device)
        self.threshold = threshold
    
    def preprocess(self, img):
        ### Adjust the raw image before inference ###
        preprocessed_img = torch.from_numpy(img).to(self.device)
        preprocessed_img = preprocessed_img.float()
        preprocessed_img /= 255.0 
        if preprocessed_img.ndimension() == 3:
            preprocessed_img = preprocessed_img.unsqueeze(0)
        return preprocessed_img


    def inference(self, preprocessed_img):
        ### Run model for inference ###
        
        pred = self.model(preprocessed_img)[0]
        pred = non_max_suppression(pred, self.threshold)
        

        return pred

    def postprocess(self, pred, img, im0):
        for i, det in enumerate(pred):
                if det is not None and len(det):
                    det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()
                    for *xyxy, conf, cls in reversed(det):
                        im0 = im0[int(xyxy[1]):int(xyxy[3]), int(xyxy[0]):int(xyxy[2])]
        
        return im0