from typing import Any
from argparse import Namespace

import torch
import cv2
import torch.utils.data
import torch.nn.functional as F
import yaml
from PIL import Image

from models.character_reader.utils import AttnLabelConverter
from models.character_reader.model import Model
from models.character_reader.dataset import AlignCollate
from utils.main_utils import is_cuda_available, is_cuda_device_valid, getdict


class Character_Reader():
    def __init__(self, config_path: str, threshold: float, cuda_device: int = -1, verbose: bool = False):

        # Set device
        self.device = torch.device(
            f"cuda:{str(cuda_device)}"
            if is_cuda_device_valid(cuda_device)
            else "cuda"
            if is_cuda_available()
            else "cpu"
        )

        self.threshold = threshold
        self.verbose = verbose
        self.conf = Namespace(**yaml.load(open(config_path, "r", encoding="utf-8"), yaml.SafeLoader))
        self.conf.rgb = bool(self.conf.rgb)
        self.verbose = verbose
        self.converter = AttnLabelConverter(character=self.conf.character, cuda_device=cuda_device)
        self.model = Model(self.conf, cuda_device).to(self.device)
        self.model.load_state_dict(torch.load(self.conf.saved_model, map_location=self.device))
        self.AlignCollate_demo = AlignCollate(
            imgH=self.conf.imgH, imgW=self.conf.imgW, keep_ratio_with_pad=self.conf.PAD
        )

    def _preprocess(self, images: list) -> Any:
        images = [cv2.cvtColor(img, cv2.COLOR_BGR2RGB) for img in images]
        if not self.conf.rgb:
            images = [Image.fromarray(img).convert("L") for img in images]
        image_tensors = self.AlignCollate_demo(images)
        return image_tensors

    def _predict(self, preprocessed_images: Any, **kwargs) -> Any:
        image = preprocessed_images.to(self.device)
        text_for_pred = (
            torch.LongTensor(preprocessed_images.size(0), self.conf.batch_max_length + 1).fill_(0).to(self.device)
        )
        with torch.no_grad():
            self.model.eval()
            preds = self.model(image, text_for_pred, is_train=False)
            preds_prob = F.softmax(preds, dim=2)
        return preds_prob

    def _postprocess(self, predictions: Any) -> tuple:
        result = {"character": [], "confidence": []}
        preds_max_prob, preds_index = predictions.max(dim=2)
        length_for_pred = torch.IntTensor([self.conf.batch_max_length] * predictions.size(0)).to(self.device)
        preds_str = self.converter.decode(preds_index, length_for_pred)

        for pred, pred_max_prob in zip(preds_str, preds_max_prob):
            pred_EOS = pred.find("[s]")
            pred = pred[:pred_EOS]  # prune after "end of sentence" token ([s])
            pred_max_prob = pred_max_prob[:pred_EOS]
            confidence_score = pred_max_prob.cumprod(dim=0)[-1].cpu().numpy()
            if confidence_score >= self.threshold:
                result["character"].append(pred)
                result["confidence"].append(confidence_score)
            if self.verbose:
                print("<character reader>")
                print(pred, confidence_score)
                print("</character reader>")
        return result
