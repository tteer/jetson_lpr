"""Main utils."""
import logging
import time
import warnings
from distutils.util import strtobool
from functools import wraps
from typing import Callable, Dict, List, Union, Tuple
from threading import Thread

import torch
import cv2
import ulid

logger = logging.getLogger(__name__)

ExecutionList = List[Union[Callable, List, Dict]]


def is_cuda_available() -> bool:
    """Return bool of CUDA availability."""
    return torch.cuda.is_available()


def is_cuda_device_valid(cuda_device: int) -> bool:
    """Return bool of CUDA device validation.

    Parameters
    ----------
    cuda_device: cuda device number
    """
    try:
        torch.cuda.get_device_name(cuda_device)
        return True
    except:
        return False



def getdict(dict_data, searchstring, default=None):
    """Get configuration from given source and key."""

    def searchdict(dict_data, searchkey, default=None):
        try:
            key = searchkey[0]
            new_searchkey = searchkey[1:]
            if len(new_searchkey) > 0:
                return searchdict(dict_data[key], new_searchkey, default=default)
            return dict_data[key]
        except (KeyError, TypeError):
            return default

    return searchdict(dict_data, searchstring.split("."), default)


def determine_writing_fps_and_num_frame(video_path: str, reading_fps: int) -> Tuple[float, int]:
    """Determine optimum writing fps and usable frame for given video to match the real-world time."""
    logger.debug(f"""Params: Video_path: {video_path} and reading_fps: {reading_fps}""")
    capture = cv2.VideoCapture(video_path)
    video_frame_count = capture.get(cv2.CAP_PROP_FRAME_COUNT)
    video_fps = capture.get(cv2.CAP_PROP_FPS)
    real_world_video_time_second = video_frame_count / video_fps

    usable_frame_count = int((reading_fps / video_fps) * video_frame_count)
    optimum_writing_fps = usable_frame_count / real_world_video_time_second
    logger.debug(
        f"""Returns: optimum_writing_fps: {optimum_writing_fps} and usable_frame_count: {usable_frame_count}"""
    )
    return optimum_writing_fps, usable_frame_count



def spawn_thread(target: Callable, *args, **kwargs) -> Thread:
    """Spawn thread with given callable. given callable should be a short and definite task."""
    thread = Thread(target=target, args=args, kwargs=kwargs)
    thread.start()
    return thread


def execute_sequence(execution_list: List[ExecutionList], break_when_fail=True):
    """Execute list of [func, args, kwargs], if current func returns nothing, next func will not be executed."""
    for (call, args, kwargs) in execution_list:
        result = call(*args, **kwargs)
        if not result:
            logger.warning(f"{call} is called with {args}, but return {result}")
            if break_when_fail:
                logger.warning("execution sequence is broken, because `break_when_fail` is enabled")
                break


def deprecating(func):
    """Warn user that given function is going to be deprecated or removed in the future."""

    def deprecating_func(*args, **kwargs):
        warnings.simplefilter("always", DeprecationWarning)  # turn off filter
        warnings.warn(
            "Call to deprecated function {}.".format(func.__name__), category=DeprecationWarning, stacklevel=2
        )
        warnings.simplefilter("default", DeprecationWarning)  # reset filter
        return func(*args, **kwargs)

    return deprecating_func


def check_rule(rules: List[Dict], follower: Dict):
    """
    Check if the given dict is following the given rules or not.

    Currently, rules must follow this format: {"object": ..., "attribute": ..., "operator": ..., "value": ...}
    follower must follow this for mat: {""attribute1": ..., "attribute2": ...}.
    """
    operators = {
        "gt": lambda a, b: a > b,
        "lt": lambda a, b: a < b,
        "eq": lambda a, b: a == b,
        "ne": lambda a, b: a != b,
    }
    return all(
        (
            operators[rule["operator"]](follower[rule["attribute"]], rule["value"])
            and follower["object"] == rule["object"]
        )
        if rule["attribute"] in follower
        else False
        for rule in rules
    )


def check_cv2_dependency(name: str) -> bool:
    """
    Check if specified dependency is ready to use for installed cv2.

    Dependency name is not case sensitive, given FFMPEG or ffmpeg get same result.
    """
    name = name.lower()
    cv2_dependency_modules = cv2.getBuildInformation().lower().split("\n\n")
    try:
        module_find_results = map(lambda x: x.find(name), cv2_dependency_modules)
        module_index = next(i for i, x in enumerate(module_find_results) if x != -1)
        raw_dependencies_list = cv2_dependency_modules[module_index].split("\n")
        dependencies = dict(map(lambda x: x.replace(":", "").split()[:2], raw_dependencies_list))
        return bool(strtobool(dependencies[name]))
    except (StopIteration, ValueError, KeyError):
        return False
